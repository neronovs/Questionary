package ru.narod.nod.questionnaire.ui.fragent_questions.presenter;

import android.os.Bundle;

import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Request;
import okhttp3.Response;
import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.network.network_provider.NetworkCreatorImpl;
import ru.narod.nod.questionnaire.network.network_provider.NetworkProvider;
import ru.narod.nod.questionnaire.network.retrofit.ApiFactory;
import ru.narod.nod.questionnaire.network.server_mocking.JsonCreator;
import ru.narod.nod.questionnaire.network.server_mocking.OkHttpResponse;
import ru.narod.nod.questionnaire.ui.fragent_questions.view.FragmentQuestion1Impl;
import ru.narod.nod.questionnaire.ui.fragent_questions.view.IFragmentQuestion1View;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

//@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
@RunWith(PowerMockRunner.class) //Using to mock a FINAL class
@PrepareForTest({FragmentQuestion1PresenterImpl.class //Using to prepare classes to use
        , FragmentQuestion1Impl.class
        , QuestionModel.class
        , NetworkProvider.class
        , NetworkCreatorImpl.class
        , JsonCreator.class
})
@PowerMockIgnore("javax.net.ssl.*")
public class FragmentQuestion1PresenterImplTest {

    private IFragmentQuestion1Presenter mPresenter;
    private IFragmentQuestion1View mView;
    private JsonCreator mJsonCreator;
    private QuestionModel mQuestionModel;
    private NetworkProvider mNetworkProvider;
    private ApiFactory apiFactory;
    private String jsonString = "{\"Questions\":[{\"question_name\":\"Do you enjoy working in a team?\",\"answers\":[{\"name\":\"Teamwork is in my blood\",\"points\":5},{\"name\":\"Yes, I do\",\"points\":3},{\"name\":\"I prefer to work alone\",\"points\":0}]},{\"question_name\":\"How long have you been working with Java?\",\"answers\":[{\"name\":\"Never\",\"points\":0},{\"name\":\"Less than 1 year\",\"points\":1},{\"name\":\"Less than 2 year\",\"points\":3},{\"name\":\"Less than 3 year\",\"points\":5}]},{\"question_name\":\"How do you feel about automated tests?\",\"answers\":[{\"name\":\"Mandatory\",\"points\":3},{\"name\":\"Waste of time\",\"points\":0}]}],\"Evaluation\":[{\"name\":\"Unfortunately we don't match!\",\"min_points\":0,\"max_points\":6},{\"name\":\"That looks good!\",\"min_points\":7,\"max_points\":10},{\"name\":\"Excellent!\",\"min_points\":11,\"max_points\":100}]}";
    private Gson gson;

    @BeforeClass //Using to make async RX call in one flow
    public static void before() {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        //prepare local model from local jsonString
        gson = new Gson();
        mQuestionModel = gson.fromJson(jsonString, QuestionModel.class);

        mockStatic(JsonCreator.class);

        mJsonCreator = new JsonCreator();

        mNetworkProvider = mock(NetworkProvider.class);
//        mNetworkProvider = new NetworkProvider();

        mView = mock(IFragmentQuestion1View.class);

        mPresenter = new FragmentQuestion1PresenterImpl(mView, mQuestionModel, mNetworkProvider);

        mockStatic(NetworkCreatorImpl.class);
        //Substitute networkCreatorImpl
        PowerMockito.when(mNetworkProvider.getQuestionaryRepository())
                .thenAnswer(invocation -> new NetworkCreatorImpl());
    }

    @Test
    public void getAnswerTest() throws Exception {
//        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());

        verifyNoMoreInteractions(mView);
        mPresenter.getAnswer();
        verify(mView, times(1)).showLoadingIndicator();
        verify(mView, times(1)).hideLoadingIndicator();
        verify(mView).showError(any()); //no connection to server

        mView.showQuestions(mQuestionModel);
        verify(mView).showQuestions(mQuestionModel);
    }

    @Test
    public void getAnswerLocalyTest() throws Exception {
        IFragmentQuestion1Presenter lPresenter = mock(FragmentQuestion1PresenterImpl.class);
        lPresenter.getAnswer();
        lPresenter.getQuestionModel();
        verify(lPresenter).getAnswer();
        verify(lPresenter).getQuestionModel();
        verify(lPresenter, times(1)).getAnswer();
        verify(lPresenter, times(1)).getQuestionModel();
    }

    @Test
    public void testCreatedTest() {

        assertNotNull(gson);
        assertNotNull(mJsonCreator);
        assertNotNull(mQuestionModel);
        assertNotNull(mNetworkProvider);
        assertNotNull(mNetworkProvider.getQuestionaryRepository());
        try {
            assertNotNull(mNetworkProvider.getQuestionaryRepository().provideQuestionaryObserver());
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertNotNull(mView);
        assertNotNull(mPresenter);
        assertNotNull(mPresenter.getQuestionModel());

        mPresenter.onStart(new Bundle());
        mPresenter.onResume();
        mPresenter.onPause();
        mPresenter.onEnd();
    }

    @Test
    public void checkIfAnswerFromNetworkproviderAndTampleSameTest() throws Exception {
        //Creating
        InputStream stream = new ByteArrayInputStream(jsonString.getBytes(StandardCharsets.UTF_8.name()));
        //Substitute inputstream with a before created to get json string from a file
        PowerMockito.when(JsonCreator.getStreamOfFile()).thenAnswer(invocation -> stream);
        Request request = new Request.Builder().url("https://google.com").build();
        Response response = OkHttpResponse.success(request, JsonCreator.getStreamOfFile());
        assertNotNull(response);

        String jsonString2 = String.valueOf(response.body().string());
        QuestionModel lQuestionModel = new Gson().fromJson(jsonString2, QuestionModel.class);
        assertNotNull(lQuestionModel);

        assertEquals(mQuestionModel.getQuestions().size(), lQuestionModel.getQuestions().size());
        assertEquals(mQuestionModel.getQuestions().get(0).getLastSelectedAnswer(),
                lQuestionModel.getQuestions().get(0).getLastSelectedAnswer());
        lQuestionModel.getQuestions().get(0).setLastSelectedAnswer(2);
        assertNotEquals(mQuestionModel.getQuestions().get(0).getLastSelectedAnswer(),
                lQuestionModel.getQuestions().get(0).getLastSelectedAnswer());
        assertEquals(mQuestionModel.getEvaluation().size(), lQuestionModel.getEvaluation().size());

    }

    @Test
    public void showResultOfQuestionaireTest() throws Exception {
        final String noAnswers = "No answered questions!";
        final String negativeAnswer = "Unfortunately we don't match!";
        final String goodAnswer = "That looks good!";
        final String excellentAnswer = "Excellent!";
        assertNotNull(mQuestionModel.getEvaluation());
        assertTrue(mQuestionModel.getEvaluation().size() == 3);
        String evaluationString = mPresenter.showResultOfQuestionaire(mQuestionModel);
        assertEquals(evaluationString, noAnswers); //no questions are answered, -3 points as default value

        mQuestionModel.getQuestions().get(0).setLastSelectedAnswer(2); //put answer for 1 question with 0 points
        mQuestionModel.getQuestions().get(1).setLastSelectedAnswer(0); //put answer for 2 question with 0 points
        mQuestionModel.getQuestions().get(2).setLastSelectedAnswer(1); //put answer for 3 question with 0 points
        evaluationString = mPresenter.showResultOfQuestionaire(mQuestionModel);
        assertEquals(evaluationString, negativeAnswer); //negative answer with 0 points, evaluation 0-6

        mQuestionModel.getQuestions().get(0).setLastSelectedAnswer(1); //put answer for 1 question with 3 points
        mQuestionModel.getQuestions().get(1).setLastSelectedAnswer(2); //put answer for 2 question with 3 point
        mQuestionModel.getQuestions().get(2).setLastSelectedAnswer(0); //put answer for 3 question with 3 points
        evaluationString = mPresenter.showResultOfQuestionaire(mQuestionModel);
        assertEquals(evaluationString, goodAnswer); //good answer with 9 points, evaluation 7-10

        mQuestionModel.getQuestions().get(0).setLastSelectedAnswer(0); //put answer for 1 question with 5 points
        mQuestionModel.getQuestions().get(1).setLastSelectedAnswer(3); //put answer for 2 question with 5 points
        mQuestionModel.getQuestions().get(2).setLastSelectedAnswer(0); //put answer for 3 question with 3 points
        evaluationString = mPresenter.showResultOfQuestionaire(mQuestionModel);
        assertEquals(evaluationString, excellentAnswer); //excellent answer with 13 points, evaluation >10

    }

    @After
    public void tearDown() throws Exception {

        //Release rx plugins
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }


}