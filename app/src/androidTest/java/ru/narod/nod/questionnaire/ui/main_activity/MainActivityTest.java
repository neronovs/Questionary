package ru.narod.nod.questionnaire.ui.main_activity;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.narod.nod.questionnaire.R;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Before
    public void setUp() throws Exception {
    }

    @Rule
    public final ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void onCreate() throws Exception {
        assertNotNull(mActivityRule);
        assertEquals(mActivityRule.getActivity().getTitle(),
                mActivityRule.getActivity().getResources().getString(R.string.app_name));
    }

    @After
    public void tearDown() throws Exception {
    }

}