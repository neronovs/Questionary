package ru.narod.nod.questionnaire.ui.fragent_questions.view;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Button;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.narod.nod.questionnaire.R;
import ru.narod.nod.questionnaire.ui.main_activity.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.isFocusable;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class FragmentQuestion1ImplTest {

    int btnFinishId;
    int btnRestartId;
    int viewPagerId;
    int rvHeaderId;
    int rvId;

    @Rule
    public final ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() {
        btnFinishId = R.id.fragment_q_btn_finish;
        btnRestartId = R.id.fragment_q_btn_restart_app;
        viewPagerId = R.id.fragment_q_pager;
        rvHeaderId = R.id.fragment_rv_header;
        rvId = R.id.fragmentet_rv_rv;
    }

    @Test
    public void checkButtonFinish() throws Exception {
        Button btnFinish = mActivityRule.getActivity().findViewById(btnFinishId);
        onView(withId(btnFinishId)).check(matches(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
                , isFocusable()
                , isClickable()
                , withText(btnFinish.getText().toString())
        )));
    }

    @Test
    public void checkButtonRestartApp() throws Exception {
        Button btnRestart = mActivityRule.getActivity().findViewById(btnRestartId);
        onView(withId(btnRestartId)).check(matches(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)
                , isFocusable()
                , isClickable()
                , withText(btnRestart.getText().toString())
        )));
    }

    @Test
    public void checkViewPager() throws Exception {
        onView(withId(viewPagerId)).check(matches(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
                , isEnabled()
                , isDisplayed()
        )));
    }

    @After
    public void tearDown() throws Exception {
    }

}