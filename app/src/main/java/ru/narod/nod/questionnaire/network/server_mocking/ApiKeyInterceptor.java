package ru.narod.nod.questionnaire.network.server_mocking;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApiKeyInterceptor implements Interceptor {

    private final RequestsHandler mHandlers;

    public ApiKeyInterceptor() {
        this.mHandlers = new RequestsHandler();
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        String path = request.url().encodedPath();

        if (mHandlers.shouldIntercept(path)) {
            return mHandlers.proceed(request, path);
        }
        return chain.proceed(request);
    }
}