package ru.narod.nod.questionnaire.ui.fragent_questions.view;

import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.questionnaire.inject.PerFragment;
import ru.narod.nod.questionnaire.ui.fragent_questions.presenter.FragmentQuestion1PresenterModule;
import ru.narod.nod.questionnaire.ui.general.view.BaseFragmentModule;

/**
 * Provides example 1 fragment dependencies.
 */
@Module(includes = {
        BaseFragmentModule.class,
        FragmentQuestion1PresenterModule.class
})
public abstract class FragmentQuestion1Module {

    /**
     * As per the contract specified in {@link BaseFragmentModule}; "This must be included in all
     * fragment modules, which must provide a concrete implementation of {@link Fragment}
     * and named {@link BaseFragmentModule#FRAGMENT}.
     *
     * @param fragmentQuestion1Impl the fragment
     * @return the fragment
     */
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(FragmentQuestion1Impl fragmentQuestion1Impl);

    @Binds
    @PerFragment
    abstract IFragmentQuestion1View iFragmentQuestion1View(FragmentQuestion1Impl fragmentQuestion1Impl);

}