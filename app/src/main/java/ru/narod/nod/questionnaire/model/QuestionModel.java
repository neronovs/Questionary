
package ru.narod.nod.questionnaire.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.inject.Inject;

public class QuestionModel {

    @Inject
    public QuestionModel() {}

    @SerializedName("Questions")
    @Expose
    private List<Question> questions = null;
    @SerializedName("Evaluation")
    @Expose
    private List<Evaluation> evaluation = null;

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<Evaluation> getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(List<Evaluation> evaluation) {
        this.evaluation = evaluation;
    }

}
