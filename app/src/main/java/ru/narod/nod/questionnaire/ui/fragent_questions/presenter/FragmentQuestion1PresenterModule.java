package ru.narod.nod.questionnaire.ui.fragent_questions.presenter;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.questionnaire.inject.PerFragment;
import ru.narod.nod.questionnaire.ui.fragent_questions.view.FragmentQuestion1Impl;

/**
 * Provides presenter dependencies.
 */
@Module
public abstract class FragmentQuestion1PresenterModule {

    /**
     * The presenter listens to the events in the {@link FragmentQuestion1Impl}.
     *
     * @param fragmentQuestion1Presenter the presenter
     * @return the FragmentQuestion1Impl listener
     */
    @Binds
    @PerFragment
    abstract IFragmentQuestion1Presenter iFragmentQuestion1Presenter(
            FragmentQuestion1PresenterImpl fragmentQuestion1Presenter);

}