
package ru.narod.nod.questionnaire.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Question {

    @SerializedName("question_name")
    @Expose
    private String questionName;
    @SerializedName("answers")
    @Expose
    private List<Answer> answers = null;

    //***Keeps last selected radiobutton (answer)
    private int lastSelectedAnswer = -1;
    public int getLastSelectedAnswer() {
        return lastSelectedAnswer;
    }
    public void setLastSelectedAnswer(int lastSelectedAnswer) {
        this.lastSelectedAnswer = lastSelectedAnswer;
    }
    //***

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

}
