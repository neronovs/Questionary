package ru.narod.nod.questionnaire.network.network_provider;

import java.io.IOException;

import ru.narod.nod.questionnaire.model.QuestionModel;

public interface INetworkCreator {

    io.reactivex.Observable<QuestionModel> provideQuestionaryObserver() throws IOException;

}
