package ru.narod.nod.questionnaire.ui.fragent_recycler_view.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import ru.narod.nod.questionnaire.R;
import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.ui.fragent_recycler_view.presenter.IFragmentRecyclerPresenter;
import ru.narod.nod.questionnaire.ui.fragent_recycler_view.presenter.RecyclerViewAdapter;
import ru.narod.nod.questionnaire.ui.general.view.BaseViewFragment;

public class FragmentRecyclerImpl
        extends BaseViewFragment<IFragmentRecyclerPresenter>
        implements IFragmentRecyclerView {

    //region Variable declaration
    private static final String NUMBER_TAG = "number";
    private static final String MAX_NUMBER_TAG = "max_page_number";

    // Store instance variables
    private int page;
    private int maxPageNumber;
    private static QuestionModel currentModel;

    private RecyclerView rv;
    private RecyclerViewAdapter mDataAdapter;

    @BindView(R.id.fragment_rv_header)
    TextView header;
    //endregion

    // newInstance constructor for creating fragment with arguments
    public static FragmentRecyclerImpl newInstance(int page, QuestionModel model) {
        currentModel = model;
        FragmentRecyclerImpl fragmentFirst = new FragmentRecyclerImpl();
        Bundle args = new Bundle();
        args.putInt(NUMBER_TAG, page);
        args.putInt(MAX_NUMBER_TAG, currentModel.getQuestions().size());
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Store instance variables based on arguments passed
        page = getArguments().getInt(NUMBER_TAG, 0);
        maxPageNumber = getArguments().getInt(MAX_NUMBER_TAG, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rv,
                container, false);

        // Inflate the view for the fragment based on layout XML
        TextView tvLabel = view.findViewById(R.id.fragment_rv_header);
        if (page < maxPageNumber)
            tvLabel.setText(currentModel.getQuestions().get(page).getQuestionName());
        else
            tvLabel.setText(R.string.evaluation);

        //Recycler view creating
        rv = view.findViewById(R.id.fragmentet_rv_rv);
        rv.setLayoutManager(new GridLayoutManager(activityContext, 1,
                GridLayoutManager.VERTICAL, false));
        mDataAdapter = new RecyclerViewAdapter(currentModel.getQuestions().get(page));
        rv.setAdapter(mDataAdapter);

        return view;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

}