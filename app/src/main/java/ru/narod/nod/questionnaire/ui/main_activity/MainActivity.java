package ru.narod.nod.questionnaire.ui.main_activity;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.optimizely.Optimizely;

import org.json.JSONException;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.HasFragmentInjector;
import io.fabric.sdk.android.Fabric;
import ru.narod.nod.questionnaire.R;
import ru.narod.nod.questionnaire.network.server_mocking.JsonCreator;
import ru.narod.nod.questionnaire.ui.fragent_questions.view.FragmentQuestion1Impl;
import ru.narod.nod.questionnaire.ui.general.BaseActivity;

public class MainActivity
        extends BaseActivity
        implements HasFragmentInjector {

    //Firebase, for analysing
    private FirebaseAnalytics mFirebaseAnalytics;

    @Inject
    JsonCreator jsonCreator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Optimizely.startOptimizelyWithAPIToken(getString(R.string.com_optimizely_api_key), getApplication());
        Fabric.with(this);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        //Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        try {
            jsonCreator.preparingJson(getApplicationContext());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (savedInstanceState == null) {
            //Adding the questionary fragment
            addFragment(R.id.main_q_container, new FragmentQuestion1Impl());
        }

    }

}
