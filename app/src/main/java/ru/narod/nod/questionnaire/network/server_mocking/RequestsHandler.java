package ru.narod.nod.questionnaire.network.server_mocking;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import okhttp3.Request;
import okhttp3.Response;

public final class RequestsHandler {

    private final Map<String, String> mResponsesMap = new HashMap<>();

    RequestsHandler() {
        final String FILE_JSON_NAME = "json_response.json";
        mResponsesMap.put("/questionary", FILE_JSON_NAME);

    }

    boolean shouldIntercept(@NonNull String path) {

        Set<String> keys = mResponsesMap.keySet();
        for (String interceptUrl : keys) {
            if (path.contains(interceptUrl)) {
                return true;
            }
        }

        return false;
    }

    @NonNull
    Response proceed(@NonNull Request request, @NonNull String path) {
        Set<String> keys = mResponsesMap.keySet();
        for (String interceptUrl : keys) {
            if (path.contains(interceptUrl)) {
                String mockResponsePath = mResponsesMap.get(interceptUrl);
                return createResponseFromAssets(request, mockResponsePath);
            }
        }

        return OkHttpResponse.error(request, 500, "Incorrectly intercepted request");
    }

    @NonNull
    private Response createResponseFromAssets(@NonNull Request request, @NonNull String assetPath) {
        try {
            //Getting a created json to mock it as a server answer
            final InputStream stream = JsonCreator.getStreamOfFile();
            //noinspection TryFinallyCanBeTryWithResources
            try {
                return OkHttpResponse.success(request, stream);
            } finally {
                stream.close();
            }
        } catch (IOException e) {
            return OkHttpResponse.error(request, 500, e.getMessage());
        }
    }

}