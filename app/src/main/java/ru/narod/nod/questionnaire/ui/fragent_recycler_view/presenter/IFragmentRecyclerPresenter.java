package ru.narod.nod.questionnaire.ui.fragent_recycler_view.presenter;


import ru.narod.nod.questionnaire.ui.general.presenter.Presenter;

/**
 * A {@link Presenter} that does some work and shows the results.
 */
public interface IFragmentRecyclerPresenter extends Presenter {

}
