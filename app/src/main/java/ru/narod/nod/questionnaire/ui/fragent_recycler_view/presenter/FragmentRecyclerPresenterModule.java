package ru.narod.nod.questionnaire.ui.fragent_recycler_view.presenter;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import ru.narod.nod.questionnaire.inject.PerFragment;
import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.ui.fragent_recycler_view.view.FragmentRecyclerImpl;

/**
 * Provides presenter dependencies.
 */
@Module
public abstract class FragmentRecyclerPresenterModule {

    /**
     * The presenter listens to the events in the {@link FragmentRecyclerImpl}.
     *
     * @param fragmentRecyclerPresenter the presenter
     * @return the FragmentRecyclerImpl listener
     */
    @Binds
    @PerFragment
    abstract IFragmentRecyclerPresenter iFragmentRecyclerPresenter(
            FragmentRecyclerPresenterImpl fragmentRecyclerPresenter);

    @Provides
    @PerFragment
    static QuestionModel questionModel() {
        return new QuestionModel();
    }


}