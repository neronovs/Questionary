package ru.narod.nod.questionnaire.ui.main_activity;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.narod.nod.questionnaire.inject.PerActivity;
import ru.narod.nod.questionnaire.inject.PerFragment;
import ru.narod.nod.questionnaire.ui.fragent_questions.view.FragmentQuestion1Impl;
import ru.narod.nod.questionnaire.ui.fragent_questions.view.FragmentQuestion1Module;
import ru.narod.nod.questionnaire.ui.fragent_recycler_view.view.FragmentRecyclerImpl;
import ru.narod.nod.questionnaire.ui.fragent_recycler_view.view.FragmentRecyclerModule;
import ru.narod.nod.questionnaire.ui.general.view.BaseActivityModule;

/**
 * Provides main activity dependencies.
 */
@Module(includes = BaseActivityModule.class)
public abstract class MainActivityModule {

    /**
     * As per the contract specified in {@link BaseActivityModule}; "This must be included in all
     * activity modules, which must provide a concrete implementation of {@link Activity}."
     * <p>
     * This provides the activity required to inject the
     * {@link BaseActivityModule#ACTIVITY_FRAGMENT_MANAGER} into the
     * {@link ru.narod.nod.questionnaire.ui.general.BaseActivity}.
     *
     * @param mainActivity the activity
     * @return the activity
     */
    @Binds
    @PerActivity
    abstract Activity activity(MainActivity mainActivity);

    /**
     * Provides the injector for the {@link FragmentQuestion1Impl}, which has access to the dependencies
     * provided by this activity and application instance (singleton scoped objects).
     */
    @PerFragment
    @ContributesAndroidInjector(modules = FragmentQuestion1Module.class)
    abstract FragmentQuestion1Impl fragmentQuestion1();

    /**
     * Provides the injector for the {@link FragmentRecyclerImpl}, which has access to the dependencies
     * provided by this activity and application instance (singleton scoped objects).
     */
    @PerFragment
    @ContributesAndroidInjector(modules = FragmentRecyclerModule.class)
    abstract FragmentRecyclerImpl fragmentRecycler();

}
