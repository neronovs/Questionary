package ru.narod.nod.questionnaire;


import android.app.Application;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;
import ru.narod.nod.questionnaire.inject.PerActivity;
import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.network.network_provider.NetworkProvider;
import ru.narod.nod.questionnaire.network.server_mocking.JsonCreator;
import ru.narod.nod.questionnaire.ui.main_activity.MainActivity;
import ru.narod.nod.questionnaire.ui.main_activity.MainActivityModule;

@Module(includes = AndroidInjectionModule.class)
abstract class AppModule {

    @Binds
    @Singleton
    /*
     * Singleton annotation isn't necessary since Application instance is unique but is here for
     * convention. In general, providing Activity, Fragment, BroadcastReceiver, etc does not require
     * them to be scoped since they are the components being injected and their instance is unique.
     *
     * However, having a scope annotation makes the module easier to read. We wouldn't have to look
     * at what is being provided in order to understand its scope.
     */
    abstract Application application(App app);

    /**
     * Provides the injector for the {@link MainActivity}, which has access to the dependencies
     * provided by this application instance (singleton scoped objects).
     */
    @ContributesAndroidInjector(modules = { MainActivityModule.class })
    @PerActivity
    abstract MainActivity mainActivityInjector();

    @Provides
    @Singleton
    static JsonCreator jsonCreator() {
        return new JsonCreator();
    }

    @Provides
    @Singleton
    static NetworkProvider networkProvider() {
        return new NetworkProvider();
    };

    @Provides
    @Singleton
    static EventBus getBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    static QuestionModel questionModel() { return new QuestionModel(); }

}