package ru.narod.nod.questionnaire.network.retrofit;

import retrofit2.http.GET;
import ru.narod.nod.questionnaire.model.QuestionModel;

public interface IQuestionaryService {

    @GET("/questionary")
    io.reactivex.Observable<QuestionModel> questionaryService();

}
