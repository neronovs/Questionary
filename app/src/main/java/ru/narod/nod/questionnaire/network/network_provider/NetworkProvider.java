package ru.narod.nod.questionnaire.network.network_provider;


import javax.inject.Inject;

public class NetworkProvider {

    private static INetworkCreator sQuestionaryRepository;

    @Inject
    public NetworkProvider() {}

    public INetworkCreator getQuestionaryRepository() {
        if (sQuestionaryRepository == null) {
            sQuestionaryRepository = new NetworkCreatorImpl();
        }
        return sQuestionaryRepository;
    }

}
