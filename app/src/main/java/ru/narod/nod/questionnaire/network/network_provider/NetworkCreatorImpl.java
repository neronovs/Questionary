package ru.narod.nod.questionnaire.network.network_provider;

import javax.inject.Inject;

import ru.narod.nod.questionnaire.inject.PerFragment;
import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.network.retrofit.ApiFactory;

@PerFragment
public class NetworkCreatorImpl
        implements INetworkCreator {

    private static final String API_ENDPOINT_URL = "https://google.com";

//    @Inject
//    private ApiFactory apiFactory;

    @Inject
    public NetworkCreatorImpl() {}

    @Override
    public io.reactivex.Observable<QuestionModel> provideQuestionaryObserver() {
        return ApiFactory.getQuestionaryService(API_ENDPOINT_URL)
                .questionaryService();
    }

}