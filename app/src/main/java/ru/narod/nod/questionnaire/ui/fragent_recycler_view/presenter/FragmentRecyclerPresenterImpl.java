package ru.narod.nod.questionnaire.ui.fragent_recycler_view.presenter;

import android.support.annotation.NonNull;

import javax.inject.Inject;

import ru.narod.nod.questionnaire.inject.PerFragment;
import ru.narod.nod.questionnaire.ui.fragent_recycler_view.view.IFragmentRecyclerView;
import ru.narod.nod.questionnaire.ui.general.presenter.BasePresenter;

@PerFragment
final public class FragmentRecyclerPresenterImpl
        extends BasePresenter<IFragmentRecyclerView>
        implements IFragmentRecyclerPresenter {

    @Inject
    FragmentRecyclerPresenterImpl(@NonNull IFragmentRecyclerView view) {
        super(view);
    }

}

