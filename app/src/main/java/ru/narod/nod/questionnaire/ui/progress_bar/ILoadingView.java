package ru.narod.nod.questionnaire.ui.progress_bar;


public interface ILoadingView {

    void hideLoadingIndicator();

    void showLoadingIndicator();

}
