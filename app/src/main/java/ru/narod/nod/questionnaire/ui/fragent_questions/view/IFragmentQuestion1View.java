package ru.narod.nod.questionnaire.ui.fragent_questions.view;

import android.support.annotation.NonNull;

import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.ui.general.view.MVPView;
import ru.narod.nod.questionnaire.ui.progress_bar.ILoadingView;

public interface IFragmentQuestion1View
        extends ILoadingView, MVPView {

    void showQuestions(@NonNull QuestionModel model);

    void showError(Throwable throwable);

    void finishQuestionary(String evaluation);

//    void showLoadingIndicator(Disposable disposable);

}