package ru.narod.nod.questionnaire.ui.fragent_questions.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.narod.nod.questionnaire.R;
import ru.narod.nod.questionnaire.bus.EventMessage;
import ru.narod.nod.questionnaire.model.Question;
import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.ui.fragent_questions.presenter.IFragmentQuestion1Presenter;
import ru.narod.nod.questionnaire.ui.fragent_recycler_view.view.FragmentRecyclerImpl;
import ru.narod.nod.questionnaire.ui.general.view.BaseViewFragment;

public class FragmentQuestion1Impl
        extends BaseViewFragment<IFragmentQuestion1Presenter>
        implements IFragmentQuestion1View {

    //region Variable declaration
    private static QuestionModel currentModel;

    private FragmentPagerAdapter adapterViewPager;
    private ViewPager vpPager;

    @BindView(R.id.fragment_q_cont_start)
    ConstraintLayout cont_start;

    @BindView(R.id.fragment_q_cont_finish)
    ConstraintLayout cont_finish;

    @BindView(R.id.fragment_q_btn_finish)
    Button btn_finish;;

    @Inject
    EventBus bus;
    //endregion

    @Inject
    public FragmentQuestion1Impl() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_questionary,
                container, false);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        cont_finish.setVisibility(View.INVISIBLE);

        try {
            presenter.getAnswer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showQuestions(@NonNull QuestionModel model) {
        initPager(model);
    }

    //Prepare pager view
    void initPager(QuestionModel model) {
        currentModel = model;
        vpPager = getActivity().findViewById(R.id.fragment_q_pager);
        adapterViewPager = new QuestionPagerAdapter(getFragmentManager(), currentModel.getQuestions().size());
        vpPager.setAdapter(adapterViewPager);
    }

    @OnClick(R.id.fragment_q_btn_finish)
    void btnFinishClick(Button btn) {
        presenter.showResultOfQuestionaire(currentModel);
    }

    @OnClick(R.id.fragment_q_btn_restart_app)
    void btnRestartClick(Button btn) {
        Intent i = getActivity().getPackageManager()
                .getLaunchIntentForPackage( getActivity().getPackageName() );
        assert i != null;
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void finishQuestionary(String evaluation) {
        cont_start.setVisibility(View.GONE);
        cont_finish.setVisibility(View.VISIBLE);

        TextView tvEvaluationResult = cont_finish.findViewById(R.id.finish_evaluation);
        tvEvaluationResult.setText(evaluation);

        YoYo.with(Techniques.Bounce) //Click animation
                .duration(500)
                .repeat(1)
                .playOn(tvEvaluationResult);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //Radiobutton's handler
    public void messageEventFromService(EventMessage event){
        boolean ifAllQuestionsAnswered = true;

        for (Question q : currentModel.getQuestions()) {
            if (q.getLastSelectedAnswer() == -1) {
                ifAllQuestionsAnswered = false;
                break;
            }
        }

        //If all questions are answered than show a finish button
        btn_finish.setText(ifAllQuestionsAnswered ? getString(
                R.string.btn_finish) : getString(R.string.btn_answer_all_questions));
        btn_finish.setEnabled(ifAllQuestionsAnswered);

        //implementation of automatic swiping due to having a choice
//        if (vpPager.getCurrentItem() < vpPager.getChildCount())
//            vpPager.setCurrentItem(vpPager.getCurrentItem() + 1);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private static class QuestionPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS;

        QuestionPagerAdapter(FragmentManager fragmentManager, int itemsNumber) {
            super(fragmentManager);
            NUM_ITEMS = itemsNumber;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            return FragmentRecyclerImpl.newInstance(position, currentModel);
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            int pageNum = position + 1;
            return "Question #" + pageNum;
        }
    }

}