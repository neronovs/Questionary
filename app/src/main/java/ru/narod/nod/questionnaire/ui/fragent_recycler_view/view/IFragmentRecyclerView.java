package ru.narod.nod.questionnaire.ui.fragent_recycler_view.view;

import ru.narod.nod.questionnaire.ui.general.view.MVPView;
import ru.narod.nod.questionnaire.ui.progress_bar.ILoadingView;

public interface IFragmentRecyclerView
        extends ILoadingView, MVPView {

}


