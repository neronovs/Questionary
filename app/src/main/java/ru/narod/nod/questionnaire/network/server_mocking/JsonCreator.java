package ru.narod.nod.questionnaire.network.server_mocking;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;

public class JsonCreator {

    private final String FILE_JSON_NAME = "json_response.json";

    private static InputStream streamOfFile;
    public static InputStream getStreamOfFile() {
        return streamOfFile;
    }

    public void preparingJson(Context context)
            throws IOException, JSONException {

        //***Creating the 1st question and answers for it
        JSONObject question1 = new JSONObject();
            JSONArray answers1 = new JSONArray();
            JSONObject answer11 = new JSONObject();
            answer11.put("name", "Teamwork is in my blood");
            answer11.put("points", 5);
            JSONObject answer12 = new JSONObject();
            answer12.put("name", "Yes, I do");
            answer12.put("points", 3);
            JSONObject answer13 = new JSONObject();
            answer13.put("name", "I prefer to work alone");
            answer13.put("points", 0);
            answers1.put(answer11).put(answer12).put(answer13);
        question1.put("question_name", "Do you enjoy working in a team?");
        question1.put("answers", answers1);
        //***

        //***Creating the 2d question and answers for it
        JSONObject question2 = new JSONObject();
            JSONArray answers2 = new JSONArray();
            JSONObject answer21 = new JSONObject();
            answer21.put("name", "Never");
            answer21.put("points", 0);
            JSONObject answer22 = new JSONObject();
            answer22.put("name", "Less than 1 year");
            answer22.put("points", 1);
            JSONObject answer23 = new JSONObject();
            answer23.put("name", "Less than 2 year");
            answer23.put("points", 3);
            JSONObject answer24 = new JSONObject();
            answer24.put("name", "Less than 3 year");
            answer24.put("points", 5);
            answers2.put(answer21).put(answer22).put(answer23).put(answer24);
        question2.put("question_name", "How long have you been working with Java?");
        question2.put("answers", answers2);
        //***

        //***Creating the 3d question and answers for it
        JSONObject question3 = new JSONObject();
            JSONArray answers3 = new JSONArray();
            JSONObject answer31 = new JSONObject();
            answer31.put("name", "Mandatory");
            answer31.put("points", 3);
            JSONObject answer32 = new JSONObject();
            answer32.put("name", "Waste of time");
            answer32.put("points", 0);
            answers3.put(answer31).put(answer32);
        question3.put("question_name", "How do you feel about automated tests?");
        question3.put("answers", answers3);
        //***


        //Creating a question list
        JSONArray questions = new JSONArray();
        questions.put(question1);
        questions.put(question2);
        questions.put(question3);

        //Creating list with evaluation data
        JSONArray evaluation = new JSONArray();
        JSONObject eval1 = new JSONObject();
        eval1.put("name", "Unfortunately we don't match!");
        eval1.put("min_points", 0);
        eval1.put("max_points", 6);
        JSONObject eval2 = new JSONObject();
        eval2.put("name", "That looks good!");
        eval2.put("min_points", 7);
        eval2.put("max_points", 10);
        JSONObject eval3 = new JSONObject();
        eval3.put("name", "Excellent!");
        eval3.put("min_points", 11);
        eval3.put("max_points", 100);
        evaluation.put(eval1).put(eval2).put(eval3);


        //The root json level
        JSONObject root = new JSONObject();
        root.put("Questions", questions);
        root.put("Evaluation", evaluation);


        saveFile(String.valueOf(root), context);
    }

    private void saveFile(String stringToFile, Context context){
        FileOutputStream fos;

        try {
            fos = context.openFileOutput(FILE_JSON_NAME, 0);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fos);

            outputStreamWriter.write(stringToFile);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

        try {
            streamOfFile = context.openFileInput(FILE_JSON_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}