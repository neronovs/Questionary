package ru.narod.nod.questionnaire.ui.fragent_questions.presenter;


import java.io.IOException;

import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.ui.general.presenter.Presenter;

/**
 * A {@link Presenter} that does some work and shows the results.
 */
public interface IFragmentQuestion1Presenter extends Presenter {

    void getAnswer() throws IOException;

    String showResultOfQuestionaire(QuestionModel model);

    QuestionModel getQuestionModel();
}
