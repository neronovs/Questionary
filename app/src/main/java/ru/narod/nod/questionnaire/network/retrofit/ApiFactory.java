package ru.narod.nod.questionnaire.network.retrofit;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.narod.nod.questionnaire.network.server_mocking.ApiKeyInterceptor;

public final class ApiFactory {

    //region Variable declaration
    private static String API_ENDPOINT_URL = null;
    private static OkHttpClient sClient;
    private static IQuestionaryService sQuestionaryService;
    //endregion

    @Inject
    ApiFactory() {}

    //Creating and returning API service
    public static IQuestionaryService getQuestionaryService(String API_ENDPOINT_URL) {
        //I know that double checked locking is not a good pattern, but it's enough here
        ApiFactory.API_ENDPOINT_URL = API_ENDPOINT_URL;
        IQuestionaryService service = sQuestionaryService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sQuestionaryService;
                if (service == null) {
                    service = sQuestionaryService = createQuestionaryService();
                }
            }
        }
        return service;
    }


    //Init an interface for the Retrofit lib
    private static IQuestionaryService createQuestionaryService() {
        return new Retrofit.Builder()
                .baseUrl(API_ENDPOINT_URL)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(IQuestionaryService.class);
    }


    //Making OkHttp instance
    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }

    //Using to putList Interceptors
    private static OkHttpClient buildClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(
//                message -> Log.i("OkHttp_Logger", message)
        );
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(new ApiKeyInterceptor())
                .build();
    }

}