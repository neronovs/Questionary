package ru.narod.nod.questionnaire.ui.fragent_recycler_view.view;

import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.questionnaire.inject.PerFragment;
import ru.narod.nod.questionnaire.ui.fragent_recycler_view.presenter.FragmentRecyclerPresenterModule;
import ru.narod.nod.questionnaire.ui.general.view.BaseFragmentModule;

/**
 * Provides example 1 fragment dependencies.
 */
@Module(includes = {
        BaseFragmentModule.class,
        FragmentRecyclerPresenterModule.class
})
public abstract class FragmentRecyclerModule {

    /**
     * As per the contract specified in {@link BaseFragmentModule}; "This must be included in all
     * fragment modules, which must provide a concrete implementation of {@link Fragment}
     * and named {@link BaseFragmentModule#FRAGMENT}.
     *
     * @param fragmentRecycler the fragment
     * @return the fragment
     */
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(FragmentRecyclerImpl fragmentRecycler);

    @Binds
    @PerFragment
    abstract IFragmentRecyclerView iFragmentRecyclerView(FragmentRecyclerImpl fragmentRecycler);

}