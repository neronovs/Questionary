package ru.narod.nod.questionnaire.ui.fragent_questions.presenter;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.narod.nod.questionnaire.inject.PerFragment;
import ru.narod.nod.questionnaire.model.Evaluation;
import ru.narod.nod.questionnaire.model.Question;
import ru.narod.nod.questionnaire.model.QuestionModel;
import ru.narod.nod.questionnaire.network.network_provider.NetworkProvider;
import ru.narod.nod.questionnaire.ui.fragent_questions.view.IFragmentQuestion1View;
import ru.narod.nod.questionnaire.ui.general.presenter.BasePresenter;

@PerFragment
final public class FragmentQuestion1PresenterImpl
        extends BasePresenter<IFragmentQuestion1View>
        implements IFragmentQuestion1Presenter {

    //region Variable declaration
//    @Inject
    private QuestionModel questionModel;

    public QuestionModel getQuestionModel() {
        return questionModel;
    }

    //    @Inject
    private NetworkProvider networkProvider;
    //endregion

    @Inject
    FragmentQuestion1PresenterImpl(@NonNull IFragmentQuestion1View view
            , QuestionModel questionModel
            , NetworkProvider networkProvider
    ) {
        super(view);
        this.questionModel = questionModel;
        this.networkProvider = networkProvider;
    }

    //Creating API request to service
    @Override
    public void getAnswer() {
        //ENDPOINT_URL is keeping in the NetworkCreatorImpl class
        try {
            networkProvider.getQuestionaryRepository()
                    .provideQuestionaryObserver()
                    .subscribeOn(Schedulers.io())
                    .map(model -> questionModel = model)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(d -> view.showLoadingIndicator())
                    .doAfterTerminate(view::hideLoadingIndicator)
                    .subscribe(view::showQuestions, view::showError);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String showResultOfQuestionaire(QuestionModel model) {
        List<Evaluation> eval = model.getEvaluation();
        int scores = countScors(model);
        String res = evaluateAnswers(eval, scores);
        view.finishQuestionary(res);

        return res; //returning for testing
    }

    private int countScors(QuestionModel model) {
        int scores = 0;
        for (Question question : model.getQuestions()) {
            if (question.getLastSelectedAnswer() < 0)
                scores += question.getLastSelectedAnswer();
            else
                scores += question.getAnswers().get(question.getLastSelectedAnswer()).getPoints();
        }
        return scores;
    }

    private String evaluateAnswers(List<Evaluation> eval, int scores) {
        if (scores < 0) return "No answered questions!";

        String resolution = "";
        for (Evaluation e : eval) {
            if (scores >= e.getMinPoints() && scores <= e.getMaxPoints())
                resolution = e.getName();
        }

        return resolution;
    }

}