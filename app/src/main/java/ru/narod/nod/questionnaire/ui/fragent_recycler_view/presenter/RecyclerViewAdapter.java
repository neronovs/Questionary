package ru.narod.nod.questionnaire.ui.fragent_recycler_view.presenter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import ru.narod.nod.questionnaire.R;
import ru.narod.nod.questionnaire.bus.EventMessage;
import ru.narod.nod.questionnaire.model.Answer;
import ru.narod.nod.questionnaire.model.Question;

public class RecyclerViewAdapter
        extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Answer> answerModels;
    private Question question;

    public RecyclerViewAdapter(Question question) {
        this.question = question;
        this.answerModels = question.getAnswers();
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_recycler_row, parent, false);

        return new RecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        //Setting answer text to a radiobutton from model
        holder.rb_answer.setText(answerModels.get(position).getName());

        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections
        holder.rb_answer.setChecked(question.getLastSelectedAnswer() == position);
    }

    @Override
    public int getItemCount() {
        return answerModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton rb_answer;

        ViewHolder(View view) {
            super(view);

            rb_answer = view.findViewById(R.id.rb_answer);

            rb_answer.setOnClickListener(v -> {
                YoYo.with(Techniques.ZoomIn) //Click animation
                        .duration(100)
                        .repeat(0)
                        .playOn(v);

                question.setLastSelectedAnswer(getAdapterPosition());
                notifyDataSetChanged();

                EventBus.getDefault().post(new EventMessage(false));
            });
        }
    }

}